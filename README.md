# WebGoat and ZAP

Run WebGoat and find vulnerabilities with ZAP cli in an automated fashion.

## set variables
```bash
TEST_TARGET="http://webgoat:8088/WebGoat"   
DAST_REPORT_FILE="dast_report.txt"  
export TEST_TARGET  
export DAST_REPORT_FILE 
```

## spin the environment
```bash
docker-compose up -d webgoat webwolf    
docker-compose run -d -e TARGET="$TEST_TARGET" -e REPORT_FILE="$DAST_REPORT_FILE" -e ENV=dev zap2docker 
```
