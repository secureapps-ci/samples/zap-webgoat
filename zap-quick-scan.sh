#!/usr/bin/env bash

target_url="${TARGET}"
results_file="${REPORT_FILE:-scan_result.txt}"

# run scanner
zap-cli \
    --verbose \
    quick-scan \
    --spider \
    --recursive \
    --ajax-spider \
    --scanners all \
    --self-contained \
    --start-options '-config api.disablekey=true' \
    "${target_url}" \
    -l Informational | tee "${results_file}"

# collect high/critical alerts
alerts=$(grep -c 'High|Critical' "${results_file}")
echo "$alerts"

# verify results
if [ "$alerts" -gt 0 ]; then
  echo "$alerts findings. Build failing"
  exit 1
else
  exit 0
  echo "success - no findings identified"
fi
